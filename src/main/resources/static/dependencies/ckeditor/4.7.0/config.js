/**
 * CKEditor Configuration
 */
CKEDITOR.editorConfig = function( config ) {
   config.defaultLanguage = 'pt';
   config.removeButtons = 'Save,Templates,NewPage,Preview,Print,HiddenField,ImageButton,Button,Select,Textarea,TextField,Radio,Form,Checkbox,Styles,Font,ShowBlocks,About,Flash,Iframe,Language,Scayt,CreateDiv';
   config.skin = 'moono-lisa';
  config.toolbarGroups = [
      { name: 'document', groups: [ 'document', 'doctools' ] },
      { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
      { name: 'forms', groups: [ 'forms' ] },
      { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
      { name: 'editing', groups: [ 'spellchecker', 'find', 'selection', 'editing' ] },
      '/',
      { name: 'styles', groups: [ 'styles' ] },
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
      { name: 'links', groups: [ 'links' ] },
      { name: 'insert', groups: [ 'insert' ] },
      { name: 'colors', groups: [ 'colors' ] },
      { name: 'tools', groups: [ 'tools' ] },
      { name: 'mode', groups: [ 'mode' ] }
   ];
   
   config.uiColor = '#E8E8E8';
   config.extraPlugins = 'lineutils,widgetselection,widget,codesnippet';
   config.codeSnippet_languages = {
            css: 'CSS',
            html: 'HTML',
            java: 'Java',
            javascript: 'JavaScript',
            json: 'JSON',
            sql: 'SQL',
            xml: 'XML',
            yaml: 'YAML'
        };
   config.codeSnippet_theme = 'routeros';
   config.height=300;
};

( function() {
	CKEDITOR.lang = {

		languages: {
			en: 1, pt:1
		},
		rtl: { ar: 1, fa: 1, he: 1, ku: 1, ug: 1 },

		load: function( languageCode, defaultLanguage, callback ) {
			if ( !languageCode || !CKEDITOR.lang.languages[ languageCode ] )
				languageCode = this.detect( defaultLanguage, languageCode );

			var that = this,
				loadedCallback = function() {
					that[ languageCode ].dir = that.rtl[ languageCode ] ? 'rtl' : 'ltr';
					callback( languageCode, that[ languageCode ] );
				};

			if ( !this[ languageCode ] )
				CKEDITOR.scriptLoader.load( CKEDITOR.getUrl( 'lang/' + languageCode + '.js' ), loadedCallback, this );
			else
				loadedCallback();
		},

		detect: function( defaultLanguage, probeLanguage ) {
			var languages = this.languages;
			probeLanguage = probeLanguage || navigator.userLanguage || navigator.language || defaultLanguage;

			var parts = probeLanguage.toLowerCase().match( /([a-z]+)(?:-([a-z]+))?/ ),
				lang = parts[ 1 ],
				locale = parts[ 2 ];

			if ( languages[ lang + '-' + locale ] )
				lang = lang + '-' + locale;
			else if ( !languages[ lang ] )
				lang = null;

			CKEDITOR.lang.detect = lang ?
			function() {
				return lang;
			} : function( defaultLanguage ) {
				return defaultLanguage;
			};
			return lang || defaultLanguage;
		}
	};
} )();