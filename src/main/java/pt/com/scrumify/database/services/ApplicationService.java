package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Resource;

public interface ApplicationService {
   Application getOne(Integer id);
   Application save(Application application);
   List<Application> listAll();
   List<Application> listAllByResource(Resource resource);
   List<Application> listAllSortedByName();
}