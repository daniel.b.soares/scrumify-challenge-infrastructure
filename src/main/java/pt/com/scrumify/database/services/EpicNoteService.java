package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.EpicNote;

public interface EpicNoteService {
   EpicNote getById(Integer id);
   EpicNote save(EpicNote epicNote);
}