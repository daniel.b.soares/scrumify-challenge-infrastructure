package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Resource;

public interface ApplicationRepository extends JpaRepository<Application, Integer> {
   
   public List<Application> findAllByOrderByNameAsc();
   List<Application> findDistinctBySubAreaTeamsMembersPkResourceOrderByNameAsc(Resource resource);
   
}