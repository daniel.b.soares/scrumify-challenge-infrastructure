package pt.com.scrumify.entities;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.annotations.NotEmpty;
import pt.com.scrumify.database.entities.Avatar;
import pt.com.scrumify.database.entities.Gender;
import pt.com.scrumify.database.entities.Profile;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.helpers.ConstantsHelper;

public class ResourceView {   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(min=3, max=3, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String abbreviation;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String name;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Gender gender = new Gender(Gender.MALE);
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Email(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMAIL + "}")
   private String email;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Avatar avatar = new Avatar();
   
   @Getter
   @Setter
   @NotEmpty(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @PastOrPresent(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_PAST_OR_PRESENT + "}")
   private Date startingDay;
   
   @Getter
   @Setter
   @FutureOrPresent(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_FUTURE_OR_PRESENT + "}")
   private Date endingDay;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(min=0, max=15, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String username;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Profile profile = new Profile();
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(min=0, max=7, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String code;
   
   @Getter
   @Setter
   @Length(min=0, max=20, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String phone;
   
   @Getter
   @Setter
   @Length(min=0, max=20, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String mobile;
   
   @Getter
   @Setter
   @Length(min=0, max=5, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String extension;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Schedule schedule = new Schedule();
   
   @Getter
   @Setter
   private boolean approved = false;
   
   @Getter
   @Setter
   private boolean active = false;
   
   public ResourceView() {
      super();
   }

   public ResourceView(Resource resource) {
      super();
      
      this.setId(resource.getId());
      this.setAbbreviation(resource.getAbbreviation());
      this.setName(resource.getName());
      this.setGender(resource.getGender());
      this.setEmail(resource.getEmail());
      this.setAvatar(resource.getAvatar());
      this.setStartingDay(resource.getStartingDay());
      this.setEndingDay(resource.getEndingDay());
      this.setUsername(resource.getUsername());
      this.setProfile(resource.getProfile());
      this.setPhone(resource.getPhone());
      this.setMobile(resource.getMobile());
      this.setExtension(resource.getExtension());
      this.setCode(resource.getCode());
      this.setApproved(resource.isApproved());
      this.setActive(resource.isActive());
      this.setSchedule(resource.getSchedule());
   }

   public Resource translate() {
      Resource resource = new Resource();
      
      resource.setId(this.getId());
      resource.setAbbreviation(this.getAbbreviation());
      resource.setName(this.getName());
      resource.setGender(this.getGender());
      resource.setEmail(this.getEmail());
      resource.setAvatar(this.getAvatar());
      resource.setStartingDay(this.getStartingDay());
      resource.setEndingDay(this.getEndingDay());
      resource.setUsername(this.getUsername());
      resource.setProfile(this.getProfile());
      resource.setPhone(this.getPhone());
      resource.setMobile(this.getMobile());
      resource.setExtension(this.getExtension());
      resource.setCode(this.getCode());
      resource.setApproved(this.isApproved());
      resource.setActive(this.isActive());
      resource.setSchedule(this.getSchedule());
      
      return resource;
   }
}