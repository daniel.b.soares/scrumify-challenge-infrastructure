package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Year;

public class YearView {
   
   @Getter
   @Setter
   private Integer id;
   
   @Getter
   @Setter
   private boolean active;
   
   public YearView() {
      super();
   }
   
   public YearView(Year year) {
      super();
      
      this.id = year.getId();
      this.active = year.isActive();
   }

   public Year translate() {
      Year year = new Year();
      
      year.setId(this.id);
      year.setActive(this.active);
      
      return year;      
   }
}