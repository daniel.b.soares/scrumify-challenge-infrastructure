package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.ApplicationCredential;
import pt.com.scrumify.database.entities.Environment;

@NoArgsConstructor
public class ApplicationCredentialView {
   @Getter
   @Setter
   private Integer id;

   @Getter
   @Setter
   private String name;

   @Getter
   @Setter
   private Integer type;
   
   @Getter
   @Setter
   private String hostname;
   
   @Getter
   @Setter
   private String port;
   
   @Getter
   @Setter
   private String username;
   
   @Getter
   @Setter
   private String password;

   @Getter
   @Setter
   private Application application = new Application();
   
   @Getter
   @Setter
   private Environment environment = new Environment();
   
   public ApplicationCredentialView(ApplicationCredential credential) {
      super();

      this.setId(credential.getId());
      this.setApplication(credential.getApplication());
      this.setEnvironment(credential.getEnvironment());
      this.setName(credential.getName());
      this.setType(credential.getType());
      this.setHostname(credential.getHostname());
      this.setPort(credential.getPort());
      this.setUsername(credential.getUsername());
      this.setPassword(credential.getPassword());
   }

   public ApplicationCredential translate() {
      ApplicationCredential credential = new ApplicationCredential();

      credential.setId(this.id);
      credential.setApplication(this.application);
      credential.setEnvironment(this.environment);
      credential.setName(this.name);
      credential.setType(this.type);
      credential.setHostname(this.hostname);
      credential.setPort(this.port);
      credential.setUsername(this.username);
      credential.setPassword(this.password);
      
      return credential;  
   }
}