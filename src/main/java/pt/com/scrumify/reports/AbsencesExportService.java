package pt.com.scrumify.reports;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.services.AbsenceService;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.entities.ReportView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Service
public class AbsencesExportService extends ExcelReportBase implements ReportExportService {
   @Autowired
   private AbsenceService absencesService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private TeamService teamsService;
   
   private static final Logger logger = LoggerFactory.getLogger(AbsencesExportService.class);

   private static final String TEMPLATE_PATH = "templates/reports/templates/Absences_Template.xlsx";
   private static final String WORKSHEET_HOME = "HOME";
   private static final String WORKSHEET_TEMPLATE = "TEMPLATE";
   
   private static final int COLOR_WHITE = 0;
   private static final int COLOR_BLACK = 1;
   private static final int COLOR_GREY_LIGHT = 2;
   private static final int COLOR_GREY = 3;
   private static final int COLOR_BLUE = 4;
   private static final int COLOR_BLUE_DARK = 5;
         
   private static final int FONT_8 = 0;
   private static final int FONT_8_BOLD = 1;
   private static final int FONT_18 = 2;
   private static final int FONT_24 = 3;

   private static final int STYLE_HOME_TITLE = 0;
   private static final int STYLE_HOME_TEXT = 1;
   private static final int STYLE_TABLE_HEADER_ALIGN_LEFT = 2;
   private static final int STYLE_TABLE_HEADER_ALIGN_CENTER = 3;
   private static final int STYLE_TABLE_ROW_ALIGN_LEFT = 4;
   private static final int STYLE_TABLE_ROW_ALIGN_CENTER = 5;
   
   private List<Resource> resources;
   private List<Team> teams;
   private Date di;
   private Date df;
   
   @Override
   public void addColors() {
      this.addColor((byte)255, (byte)255, (byte)255);
      this.addColor((byte)0, (byte)0, (byte)0);
      this.addColor((byte)242, (byte)242, (byte)242);
      this.addColor((byte)191, (byte)191, (byte)191);
      this.addColor((byte)0, (byte)132, (byte)204);
      this.addColor((byte)0, (byte)28, (byte)68);
   }

   @Override
   public void addFonts() {
      this.addFont(REPORT_FONT_NAME, (short) 8, false, false, colors.get(COLOR_BLACK));
      this.addFont(REPORT_FONT_NAME, (short) 8, true, false, colors.get(COLOR_BLUE));
      this.addFont(REPORT_FONT_NAME, (short) 18, false, false, colors.get(COLOR_BLACK));
      this.addFont(REPORT_FONT_NAME, (short) 24, true, false, colors.get(COLOR_WHITE));
   }

   @Override
   public void addStyles() {
      /*
       * STYLE_HOME_TITLE
       */
      this.addStyle(false, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_BLUE_DARK), fonts.get(FONT_24));
      
      /*
       * STYLE_HOME_TEXT
       */
      this.addStyle(false, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_18));
      
      /*
       * STYLE_TABLE_HEADER_ALIGN_LEFT
       */
      this.addStyle(false, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_8_BOLD), null, null, null, null, BorderStyle.THIN, colors.get(COLOR_BLUE_DARK), null, null);
      
      /*
       * STYLE_TABLE_HEADER_ALIGN_CENTER
       */
      this.addStyle(false, HorizontalAlignment.CENTER, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_8_BOLD), null, null, null, null, BorderStyle.THIN, colors.get(COLOR_BLUE_DARK), null, null);
      
      /*
       * STYLE_TABLE_ROW_ALIGN_LEFT
       */
      this.addStyle(true, HorizontalAlignment.LEFT, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_8), null, null, null, null, BorderStyle.DOTTED, colors.get(COLOR_GREY), null, null);
      
      /*
       * STYLE_TABLE_ROW_ALIGN_CENTER
       */
      this.addStyle(true, HorizontalAlignment.CENTER, VerticalAlignment.CENTER, FillPatternType.SOLID_FOREGROUND, colors.get(COLOR_GREY_LIGHT), fonts.get(FONT_8), null, null, null, null, BorderStyle.DOTTED, colors.get(COLOR_GREY), null, null);
   }

   @Override
   public void filters(ReportView view) {
      if ((view.getResources() == null || view.getResources().isEmpty()) && (view.getTeams() == null || view.getTeams().isEmpty())) {
         List<Team> teams = teamsService.getByMemberAndReviewerOrApprover(ResourcesHelper.getResource());
         List<Resource> resources = resourcesService.getByTeams(teams);
         
         if (resources == null || resources.size() == 0) {
            resources = new ArrayList<>();
            resources.add(ResourcesHelper.getResource());
         }
         this.resources = resources;
         this.teams = teams;
      }
      else if (view.getResources() == null || view.getResources().size() == 0) {
         List<Resource> resources = resourcesService.getByTeams(view.getTeams());
         
         if (resources == null || resources.size() == 0) {
            resources = new ArrayList<>();
            resources.add(ResourcesHelper.getResource());
         }
         this.resources = resources;
         this.teams = view.getTeams();
      }
      else if (view.getTeams() == null || view.getTeams().size() == 0) {
         this.resources = view.getResources();
      }      
      
      this.di = view.getStartDay();
      this.df = view.getEndDay();
   }
   
   @Override
   public ByteArrayInputStream export(ReportView view) {
      ClassPathResource file = new ClassPathResource(TEMPLATE_PATH);
      
      /*
       * Init
       */
      this.init();
      
      /*
       * Open template workbook
       */
      try {
         this.workbook = new XSSFWorkbook(file.getInputStream());
      }
      catch (IOException e) {
         logger.info("#internal.error #exception {}.", e.getMessage());
      }
      
      /*
       * Add colors
       */
      addColors();
      
      /*
       * Add fonts
       */
      addFonts();
      
      /*
       * Add styles
       */
      addStyles();
      
      if (this.workbook != null) {
         /*
          * Get filters
          */
         filters(view);
         
         /*
          * Write homesheet data
          */
         writeHome(view);
         
         /*
          * Write report content
          */
         writeContent(view);
         
         ByteArrayOutputStream output = new ByteArrayOutputStream();

         try {
            this.workbook.write(output);
            this.workbook.close();
         }
         catch (IOException e) {
            logger.info("#internal.error #exception {}.", e.getMessage());
         }
         
         return new ByteArrayInputStream(output.toByteArray());
      }
      
      return null;
   }

   @Override
   public void writeHome(ReportView view) {
      /*
       * WORKSHEET
       */
      this.worksheet = this.workbook.getSheet(WORKSHEET_HOME);
            
      /*
       * DATA DE EXTRAÇÃO
       */
      SimpleDateFormat sdf = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      write(COLUMN_D, ROW_023, styles.get(STYLE_HOME_TEXT), sdf.format(DatesHelper.now()));
      
      /*
       * RESOURCES
       */
      write(COLUMN_D, ROW_021, styles.get(STYLE_HOME_TEXT), getResourcesText());
      
      /*
       * TEAMS
       */
      write(COLUMN_D, ROW_019, styles.get(STYLE_HOME_TEXT), getTeamsText());
   }

   @Override
   public void writeContent(ReportView view) {
      /*
       * Create worksheet
       */
      this.worksheet = this.workbook.cloneSheet(workbook.getSheetIndex(WORKSHEET_TEMPLATE), getSheetName());
      
      /*
       * Write page title
       */
      write(COLUMN_A, ROW_002, styles.get(STYLE_HOME_TITLE), getSheetTitle(), (float) 45, null);
      
      /*
       * Write table header
       */
      tableHeader();
      
      /*
       * Set initial row
       */
      this.row = ROW_005;
      
      /*
       * Write absences
       */
      absences();
   }
   
   private void absences() {
      List<Absence> absences = absencesService.getAllBetween(this.di, this.df, this.resources);
      for (Absence absence : absences) {         
         /*
          * Write epic to report
          */
         tableRow(absence.getResource().getName(), 
                  absence.getDay().getDate(), 
                  absence.getHours(), 
                  absence.getType().getName(), 
                  absence.getJustification());
      }
   }
   
   private String getSheetName() {
      String result = "Ausências";
      
      return result.toUpperCase();
   }
   
   private String getSheetTitle() {
      String result = "                              ";
   
      result += "Ausências";
      
      return result.toUpperCase();
   }
   
   private String getResourcesText() {
      String result = "";
      
      for (Resource resource : this.resources) {
         if (result != "")
            result += ", ";
         
         result += resource.getName();
      }
      
      return result;
   }
   
   private String getTeamsText() {
      String result = "";
      
      for (Team team : this.teams) {
         if (result != "")
            result += ", ";
         
         result += team.getName();
      }
      
      return result;
   }
   
   private void tableHeader() {
      write(COLUMN_A, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_LEFT), "NOME");
      write(COLUMN_B, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_CENTER), "DIA");
      write(COLUMN_C, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_CENTER), "HORAS");
      write(COLUMN_D, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_LEFT), "TIPO DE AUSÊNCIA");
      write(COLUMN_E, ROW_004, styles.get(STYLE_TABLE_HEADER_ALIGN_LEFT), "COMENTÁRIOS");
   }
   
   private void tableRow(String resource, Date day, int hours, String type, String comments) {
      SimpleDateFormat sdf = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATE);
      
      write(COLUMN_A, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_LEFT), resource);
      write(COLUMN_B, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_CENTER), day != null ? sdf.format(day) : null);
      write(COLUMN_C, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_CENTER), hours);
      write(COLUMN_D, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_LEFT), type);
      write(COLUMN_E, this.row, styles.get(STYLE_TABLE_ROW_ALIGN_LEFT), comments);
      
      this.row++;
   }
}