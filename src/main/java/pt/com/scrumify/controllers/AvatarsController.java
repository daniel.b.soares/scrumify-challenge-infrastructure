package pt.com.scrumify.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Avatar;
import pt.com.scrumify.database.services.AvatarService;
import pt.com.scrumify.entities.PersonalData;
import pt.com.scrumify.entities.SignUp;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class AvatarsController {
   @Autowired
   private AvatarService avatarsService;

   /*
    * AVATARS
    */
   @ApiOperation(produces = ConstantsHelper.MIME_TYPE_HTML, value = "Mapping to handle with avatars ajax requests from signup page.")
   @PostMapping(value = ConstantsHelper.MAPPING_PUBLIC_AVATARS_SIGNUP_AJAX)
   public String signUpAvatars(Model model, SignUp signUp) {
      List<Avatar> avatars = this.avatarsService.getByGender(signUp.getGender());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, signUp);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AVATARS, avatars);

      return ConstantsHelper.VIEW_FRAGMENT_AVATARS;
   }

   @ApiOperation(produces = ConstantsHelper.MIME_TYPE_HTML, value = "Mapping to handle with avatars ajax requests from personal data page.")
   @PostMapping(value = ConstantsHelper.MAPPING_PUBLIC_AVATARS_PERSONALDATA_AJAX)
   public String personalDataAvatars(Model model, PersonalData personalData) {
      List<Avatar> avatars = this.avatarsService.getByGender(personalData.getGender());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, personalData);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AVATARS, avatars);

      return ConstantsHelper.VIEW_FRAGMENT_AVATARS;
   }
}