# Cluster Setup

Here, you will create and configure a GKE cluster on GCP which will be used to hold the scrumify application.

## Terraform

Provision a (Google Kubernetes Engine) GKE cluster on GCP with Terraform using the proper terraform commands respecting the following properties:

### Provider Google
Specify the cloud provider and other variables
```sh
Provider: google
version: "3.5.0"
project: <project_id>
region: <project_region>
zone: <project_zone>
batching: enable_batching set to false
```
Documentation:  
https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference

### Resource google project service
Activate a list of google services
```sh
resource type: "google_project_service"
services: "compute.googleapis.com", 
    "iam.googleapis.com",
    "cloudbuild.googleapis.com", 
    "containerregistry.googleapis.com",
	"container.googleapis.com"
disable_on_destroy: false
```
Documentation:  
https://www.terraform.io/docs/language/resources/syntax.html  
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_project_service

### Resource google kubernetes cluster
Create kubernetes cluster
```sh
resource type: "google_container_cluster"
name: <cluster_name>
location: <project_zone>

network: google_compute_network.demo_environments.id  
subnetwork: google_compute_subnetwork.public_subnet.id

remove_default_node_pool: true
initial_node_count: 1

depends_on: [google_project_service.service]
```
Documentation:  
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster
https://www.terraform.io/docs/language/meta-arguments/depends_on.html

### Resource node pool
Specify the details of the cluster's node pool
```sh
resource type: "google_container_node_pool"
name: <node_pool_name>
location: <project_zone>

cluster: google_container_cluster.java_migration_kubernetes_cluster.name
node_count: 2

node_config: 
    preemptible set to false and machine_type to "e2-medium"
    metadata: disable-legacy-endpoints set to "true"
    oauth_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

```
Documentation:  
https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster


## Fetch credentials for a running cluster

Before start doing modifications into the cluster, this is issuing ctl commands, we need to make sure which cluster are we talking to.  
Issue the command: `gcloud container clusters get-credentials <cluster_name> --region <project_zone> --project <project_id>`

## Istio

Istio manages traffic flows between services, enforces access policies, and aggregates telemetry data, all without requiring changes to application code.

In this step we will need to deploy Istio in the cluster using the following switches:
```sh
--set profile=demo
--skip-confirmation
```
Documentation: https://istio.io/latest/docs/reference/commands/istioctl/
More about Istio: https://istio.io/latest/about/service-mesh/

### Istio Addons

These addons are not the focus of this challenge, but do have interesting functionalities that can be explored.
Deploy 4 Istio addons into the cluster.

```sh
1. To understand how to apply a configuration in kubernetes:  
https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply

2. Use the following files as configurations to be applied: 
Prometheus: https://raw.githubusercontent.com/istio/istio/release-1.8/samples/addons/prometheus.yaml  
Jaeger: https://raw.githubusercontent.com/istio/istio/release-1.8/samples/addons/jaeger.yaml  
Grafana: https://raw.githubusercontent.com/istio/istio/release-1.8/samples/addons/grafana.yaml  
Kiali: https://raw.githubusercontent.com/istio/istio/release-1.8/samples/addons/kiali.yaml  
e.g.:  
    kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.8/samples/addons/prometheus.yaml
```

Add the resources to your addons declarated on the files above:
```sh
istio/istio-grafana.yaml
istio/istio-kiali.yaml
istio/istio-prometheus.yaml

1. Start by replacing the string "##CLUSTER_NAME##" by <cluster_name> in every file. You can do it by using sed command (sed -i 's/<old_string>/<new_string>/g' namespace.yaml)
2. Then apply these files into the cluster
```

## Postgres
In this step we will need to configure our Postgres database in our cluster, which will be responsible for persisting the data from scrumify application.
Thus, we need to create several kubernetes objects. 
Each object's configuration file (.yaml files) already exist, you just need to take a look, modify them, if necessary, and deploy them.

### Namespace

Namespace are used to create virtual divisions inside the cluster where you can store other kubernetes resources.
It's intended to create a namespace that will hold every object related with the scrumify application, including its database.
```sh
File: namespace.yaml
1. Start by replacing the string "##ENVIRONMENT_NAME##" by the string "<profile>-<cluster_name>", where profile can take the values "dev" or "qa".
2. Then apply this file into the cluster
```
K8's namespace documentation: https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/


### Secret

A Secret is an object that contains a small amount of sensitive data.
In this case, we need a secret to hold the postgres name, user and password.
```sh
File: postgres/postgres-secret.yaml
1. Start by replacing the string "##ENVIRONMENT_NAME##" by the same string as before
2. Replace the string "##POSTGRES_PASSWORD##" by a password of your choice
3. Then apply this file in the namespace previously created
    e.g.: kubectl apply -f <file_name> -n <namespace_name>
```
K8's secrets documentation: https://kubernetes.io/docs/concepts/configuration/secret/

### Persistent Volume / Persistent Volume Claim

Persistent volume is a piece of storage in the cluster. Persistent volume claim is a request for storage.
These objects will be required to persist the data from scumify.
```sh
File: postgres/postgres-storage.yaml
1. Start by replacing the string "##ENVIRONMENT_NAME##"
2. Then apply this file in the namespace previously created
```
K8's persisten volumes/persistent volume claim documentation: https://kubernetes.io/docs/concepts/storage/persistent-volumes/

### Deployment

A deployment object serves to tell kubernetes how to create or modify instances of the pods that hold a containerized application.
In this case we are defining 1 instance to hold postgres database. Additionally we declare which persistent volume claim to use, the volume mount path and the environment variables to input from the secret object.

```sh
File: postgres/postgres-deployment.yaml
1. Start by replacing the string "##ENVIRONMENT_NAME##"
2. Then apply this file in the namespace previously created
```
K8's deployment documentation: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/

### NodePort Service

A service is require to expose postgres database running as a network service.
A service is associated with the previous deployment object created, which is declared on the selector field.
The type of this service is set to NodePort.

```sh
File: postgres/postgres-service.yaml
1. Apply this file in the namespace previously created
```
K8's service documentation: https://kubernetes.io/docs/concepts/services-networking/service/


### Internal Load Balancer Service

This is a service kubernetes object with the type of Internal Load Balancer.

```sh
File: postgres/postgres-service.yaml
1. Apply this file in the namespace previously created
```
K8's service documentation: https://kubernetes.io/docs/concepts/services-networking/service/

## Create "scrumify" schema on postgres

```sh
Get the name of your postgres pod by issuing on the command line:
    kubectl --namespace=<namespace_name> get pods -l app=postgres

Enter in the respective pod:
    kubectl exec --namespace=<namespace_name> --stdin --tty <pod_name> -- /bin/bash

Open postgres interactive terminal:
    psql -h localhost -U postgresadmin -p 5432 postgresdb

Create a new schema by the name of "scrumify":
    CREATE SCHEMA IF NOT EXISTS scrumify;

Change new schema owner to postgresadmin:
    ALTER SCHEMA scrumify OWNER TO postgresadmin;
```


The infrastructure configuration ends here. This configuration should be performed once in your gcp project, however its a good practice to make a script containing all these instructions. If in the future there's a need to deploy this infrastructure in other project, you just need to run the script.
Tip: For each file that required any modification, one thing you can do is to create a copy, change the copy and apply it to the kubernetes and finally delete it.
